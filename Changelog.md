0.0.6
-----

* Fix bug on set subtitle_dir.
* Fix bug on set peerflix_options.
* Fix bug with secondary_subtitle.

0.0.5
-----

* Configuration on database.
* Many bug fixes.

0.0.4
-----

* Add support for search series by name.
* Add support for notify.
* Increase timeout by default.
* PREFEREDHD is 'false' by default now.

0.0.3
-----

* Fix `MARKAUTO` bug.
* Play specific episodes.
* New output format for update action.
* Add `FORCE_ALL` variable.
* Fix parsing problems.

0.0.2
-----

* Add support for a secondary subtitle.
* Add `SUBTITLE_FAIL` variable option.
* Add the number of pending episodes on `peertv play` output.

0.0.1
-----

* Initial release.
