PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin

VERSION = 0.0.6
PACKAGE = "peertv-$(VERSION).tar.gz"
FILES = AUTHORS \
		COPYING \
		Makefile \
		peertv \
		peertv.bash_completion \
		README.md \
		Changelog.md

ifeq ($(PREFIX),/usr)
	SYSCONFDIR=/etc
else
	ifeq ($(PREFIX),/usr/local)
		SYSCONFDIR=/etc
	else
		SYSCONFDIR=$(PREFIX)/etc
	endif
endif

.PONHY: install uninstall pkg

install: peertv peertv.bash_completion
	install -d $(DESTDIR)$(BINDIR)
	install -m 755 peertv $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(SYSCONFDIR)/bash_completion.d
	install -m 644 peertv.bash_completion $(DESTDIR)$(SYSCONFDIR)/bash_completion.d/peertv

uninstall:
	rm -f $(BINDIR)/peertv
	rm -f $(SYSCONFDIR)/bash_completion.d/peertv

$(PACKAGE): $(FILES)
	tar czf $@ $(FILES)

pkg: $(PACKAGE)
