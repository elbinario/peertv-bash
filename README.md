PeerTV
======

_PeerTV_ is a command line tool for manage and play series from
[EZTV](http://eztv.it).

<div class="alert alert-danger">
<em>PeerTV</em> uses <em>Peerflix</em> to stream TV shows from torrents and that may be
illegal in your country. Use at your own risk.
</div>

Dependencies
------------
* `bash`
* `sed`
* `wget`
* `tr`
* `sqlite3`
* `peerflix`
* `mpv` _(Or another player)_
* `subliminal` _(version >= 0.8.0-dev, Optional)_
* `notify-send` _(Optional)_

Install
-------
You can install the script on your system with:

    # PREFIX="/usr/local" make install

Usage
-----
You can execute the script with:

    $ peertv <action>

### Actions

* Search for a series:

        $ peertv search <string>

* Add a series to database:

        $ peertv add <shortName> <url>

    Examples:

        $ peertv add who https://eztv.it/shows/82/doctor-who-2005/
        $ peertv add who shows/82/doctor-who-2005

    Where `shortName` is a small identify text for a TV Show and `url` is the
page of the _TV show_ on _EZTV_.

* Removes a series from database:

        $ peertv del <shortName|@id>

    Examples:

        $ peertv del who # Delete "Doctor Who" series from database.
        $ peertv del @1 # Same but use series ID.

* List the series from database:

        $ peertv list

* Update the series with magnets from _EZTV_ page.

        $ peertv update <all|shortName|@id>

    Examples:

        $ peertv update all # Update all series.
        $ peertv update who # Update only "Doctor Who" series.
        $ peertv update @1 # Same but use series ID.

    If all episodes from EZTV are not added you can try with `FORCE_ALL=yes`
    variable:

        $ FORCE_ALL=yes peertv update who

* List episodes of a series.

        $ peertv list <shortName|@id>

* Mark episode as viewed.

        $ peertv mark <shortName|@id> <episode>

    Examples:

        $ peertv mark who 1x1 # Mark only 1x1 as viewed.
        $ peertv mark who "<2x2" # Mark all episodes previus to 2x2.
        $ peertv mark who ">2x2" # Mark all episodes later to 2x2.

    You can use '<' or '>' prefix for mark all previous or later episodes.

* Unmark episode as viewed.

        $ peertv unmark <shortName|@id> <episode>

    Same format as `mark`.

* Play next pending episode of series with `peerflix`.

        $ peertv play <shortName|@id>

* Play a specific episode of series.

        $ peertv play <shortName|@id> <episode>

* List series with pending episodes.

        $ peertv play

* Show help message.

        $ peertv help

Configuration
--------------
You can configure some options with `set` action.

        $ peertv set <configuration> <value>

Some configurations are:

* `eztv_url`: URL for _EZTV_.
* `mplayer_command`: Commnand for call to multimedia player.
* `mplayer_options`: Options for multimedia player.
* `mplayer_secondary_subtitle_options`: Options for play secondary subtitle.
* `mplayer_subtitle_options`: Options for play subtitle on player.
* `notify_options`: Options for notify_send.
* `notify_summary`: Summari for notifications.
* `peerflix_options`: Options for peerflix.
* `preferedhd`: Play 720p videos when available if true.
* `secondary_subtitle`: Language code for secondary subtitle or `none` for disable it.
* `subtitle_dir`: Directory for save the subtitles.
* `subtitle_timeout` Maximum time to find subtitles.
* `automark`: Automatically mark the episodes after play it if `true`.
* `notify`: Notify for news episodes if `true`.
* `subtitle_fail`: `error` and exit when subtitle download fail or only `warning`.
* `subtitle`: Language code for subtitle or `none` for disable it.
* `peerflix_timeout`: Maximum time for waiting peerflix output.

For displays all configurations and values:

    $ peertv config

